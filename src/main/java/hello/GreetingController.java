package hello;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GreetingController
{

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/greeting")
    public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name)
    {
        return new Greeting(counter.incrementAndGet(), String.format(template, name));
    }

    @RequestMapping("/state")
    public Consola state(@RequestParam(value = "tipo", defaultValue = "naranja") String dato,
                         @RequestParam(value = "objecto", defaultValue = "MW") String objeto,
                         @RequestParam(value = "papa", defaultValue = "manuel") String padre)
    {

        return new Consola(objeto, dato + " " + padre);
    }


}