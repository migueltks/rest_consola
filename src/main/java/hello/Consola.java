package hello;

/**
 * Created by migue on 18/10/2016.
 */
public class Consola
{
    private String objeto;
    private String estado;
    private String respuesta;
    private String meVale;

    public Consola (String objeto, String estado)
    {
        switch (estado)
        {
            case "INFO":
                respuesta = "Me estas pidiendo info de: " + objeto;
                break;
            case "WARN":
                respuesta = "Me estas pidiendo warn de: " + objeto;
                break;
            default:
                respuesta = "Me estas pidiendo error de: " + objeto;
                break;
        }
        this.objeto = objeto;
        this.estado = estado;
        this.meVale = "no se que quieres con este campo";
    }

    public String getObjeto()
    {
        return objeto;
    }

    public String getEstado()
    {
        return estado;
    }

    public String getRespuesta()
    {
        return respuesta;
    }

    public String getMeVale()
    {
        return meVale;
    }
}
